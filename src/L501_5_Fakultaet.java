/**
* Arbeitsauftrag-LS01-5 Aufgabe 2
*
* @author  MuFU
* @version 1.0
*/
public class L501_5_Fakultaet {
	
	public static void main(String[] args) {
		/*
		 * Aufgabe 2 Version: ohne leerstellen im string
		 * hier wird jedes einzelne symbol formatiert
		 * kann eventuell nicht genau das sein was 
		 * er als l�sung will
		 * */
		
		//Aufgabe 2 Version: ohne leerstellen im string
		System.out.printf("%-5s%s%19s%s%4s%n", "0!", "=", "","=", "1");
		System.out.printf("%-5s%-2s%-18s%s%4s%n", "1!", "=", "1","=", "1");
		System.out.printf("%-5s%-2s%-2s%-2s%-14s%s%4s%n", "2!", "=", "1", "*", "2","=", "2");
		System.out.printf("%-5s%-2s%-2s%-2s%-2s%-2s%-10s%s%4s%n", "3!", "=", "1", "*", "2", "*", "3","=", "6");
		System.out.printf("%-5s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%-6s%s%4s%n", "4!", "=", "1", "*", "2", "*", "3", "*", "4","=", "24");
		System.out.printf("%-5s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%s%4s%n", "5!", "=", "1", "*", "2", "*", "3", "*", "4", "*", "5","=", "120");
		
		/*
		 * Aufgabe 2 Version: leerstellen im string
		 * wenn er genau die 3 angegebenen Zahlen (5, 19, 4) sehen will 
		 * w�re das hier "eher" richtig. 
		 * */
		System.out.printf("%-5s%s%-19s%s%4s%n", "0!", "=", "", "=", "1");
		System.out.printf("%-5s%s%-19s%s%4s%n", "0!", "=", " 1 ", "=", "1");
		System.out.printf("%-5s%s%-19s%s%4s%n", "1!", "=", " 1 * 2 ", "=", "2");
		System.out.printf("%-5s%s%-19s%s%4s%n", "2!", "=", " 1 * 2 * 3 ", "=", "6");
		System.out.printf("%-5s%s%-19s%s%4s%n", "3!", "=", " 1 * 2 * 3 * 4 ", "=", "24");
		System.out.printf("%-5s%s%-19s%s%4s%n", "4!", "=", " 1 * 2 * 3 * 4 * 5 ", "=", "120");
		
		
		/*
		 * Fazit: da Tennbush(?) krank ist wissen wir leider gottes auch nicht was er genau will als l�sung
		 * benutzt das hier mit vorsicht
		 * MuFu out 
		 * */
	}
}
